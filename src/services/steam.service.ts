import axios from 'axios';

class SteamService {
    public get(ids: number[]) {
        const params = new URLSearchParams();
        params.append('itemcount', ids.length + '');
        ids.forEach((id, i) => {
            params.append(`publishedfileids[${i}]`, id + '');
        });
        console.log(params);
        return axios.post(
            'https://cors.bridged.cc/https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/',
            params,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'SteamDownloader',
                },
            },
        );
    }
}

export default new SteamService();
